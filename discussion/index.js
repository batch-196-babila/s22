// Array Methods
// JS jas built in methods to manipulate/manage our arrays

// Mutator methods - updates/mutates our array
// .push() - method allows to add an item at the end of array and returns a value updated length of array

let koponanNiEugene = ["Eugene"];

koponanNiEugene.push("Vincent");
console.log(koponanNiEugene);

console.log(koponanNiEugene.push("Dennis"));
console.log(koponanNiEugene);

// .pop() - method allows to remove an item at the end of array and return the item that was deleted
let removedItem = koponanNiEugene.pop();
console.log(koponanNiEugene);
console.log(removedItem);

// .unshift() - method allows to add an item at the front of array
let fruits = ["Mango", "Kiwi", "Apple"];
fruits.unshift("Pineapple");
console.log(fruits);

// .shift() - method allows to remove an item at the beginning of our array
let computerBrand = ["Apple", "Acer", "Asus", "Dell"];
computerBrand.shift();
console.log(computerBrand);

// .splice() - simultaneously remove elements from specified index and add elements
// .splice(starting index) all items from the starting index will be delited
computerBrand.splice(1);
console.log(computerBrand);

// splice(startingIndex, deleteCount) - deletes a specified number of item from a starting index
fruits.splice(0, 1);
console.log(fruits);

// element to be added from starting index
// Eugene - insert Dennis & Alfred - Vincent
koponanNiEugene.splice(1, 0, "Dennis", "Alfred"); 
console.log(koponanNiEugene);

// splice(startingIndex, deleteCount, elementsToBeAdded)
fruits.splice(0, 2, "Lime", "Cherry");
console.log(fruits);

// splice, when removing any elemetns, will be able to return removed elements
let item = fruits.splice(0);
console.log(fruits);
console.log(item);

let spiritDetective = koponanNiEugene.splice(0, 1);
console.log(spiritDetective);
console.log(koponanNiEugene);

// .sort() - method that sorts items in alphanumeric order
// used mostly for sorting arrays of strings
let members = ["Ben", "Alan", "Alvin", "Jino", "Tine"];
members.sort();
console.log(members);

// performs differently when sorting numbers
let numbers = [50, 100, 12, 10, 1];
numbers.sort();
console.log(numbers);

// .reverse() - reverse order of array
members.reverse();
console.log(members);

// NON-MUTATOR METHODS
// are not able to modify or change original array items

let carBrands = [
    "Vios", "Fortuner", "Crosswind", "City", "Vios", "Starex"
];

// indexOf()
// return the index number of first matching element in array
// If no match - returns -1

let firstIndexOfVios = carBrands.indexOf("Vios");
console.log(firstIndexOfVios);

// useful when finding index of an item when array total is unknown or constantly being added into
let indexOfStarex = carBrands.indexOf("Starex");
console.log(indexOfStarex); // 0

let indexOfBeetle = carBrands.indexOf("Beetle"); // 5
console.log(indexOfBeetle); // -1 no match found

// lastIndexOf() - returns last matching element

let lastIndexOfVios = carBrands.lastIndexOf("Vios");
console.log(lastIndexOfVios);

let indexOfMio = carBrands.lastIndexOf("Mio");
console.log(indexOfMio); // -1 no match found

// slice()
// copy a slice/portion of an array and return new array from it
// does not update original array
let shoeBrand = ["Jordan", "Nike", "Adidas", "Converse", "Sketchers"];

// copy from starting index 
// result Nike ... Sketchers
let myOwnedShoes = shoeBrand.slice(1);
console.log(myOwnedShoes);
console.log(shoeBrand); // no changes made

let herOwnShoes = shoeBrand.slice(2, 4); // Adidas Converse
console.log(herOwnShoes);
console.log(shoeBrand); 

let heroes = ["Captain America", "Superman", "Spiderman", "Wonderwoman", "Hulk", "Hawkeye", "Dr. Strange"];

let myFavoriteHeroes = heroes.slice(2, 6); // Spiderman - Hawkeye
console.log(myFavoriteHeroes); 
console.log(heroes); 

// toString() - return string  of array separated by comma

let superHeroes = heroes.toString();
console.log(superHeroes);

console.log("My favorite heroes are " + superHeroes)

// join() - returns array as string by specified operator

// withtout a specified separator (default comma)
let superHeroes2 = heroes.join();
console.log(superHeroes2);

let superHeroes3 = heroes.join(", ");
console.log(superHeroes3);

let superHeroes4 = heroes.join(1);
console.log(superHeroes4);

// ITERATOR METHODS
// Iteration - single loop
// iterates/loops over the items in an array

// forEeach() - similar to for loop 
// able to repeat an action FOR EACH item in array

// forEeach() takes an argument which is a function. This function has no name and cannot be invoked outside forEach() - anonymous
// function inside forEach() is able to receuve the current item being looped

let counter = 0;
heroes.forEach(function(hero) {
    counter++; 
    console.log(counter);
    console.log(hero);
    // console.log(counter + " " + hero);
});

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

chessBoard.forEach(function(row){
    //console.log(row);
    row.forEach(function(square) {
        console.log(square);
    });
});

let numArr = [5, 12, 30, 46, 40];

numArr.forEach(function(number){
    if (number % 5 === 0) {
        console.log(number + " is divisible by 5");
    }
    else {
        console.log(number + " is not divisible by 5");
    }
});

// map() - similar to forEach it will iterate over all items in an array and run a function
// However, whatever is returned in the function will be added into a new array and we can save

// let members = ["Ben", "Alan", "Alvin", "Jino", "Tine"];

let instructors = members.map(function(member) {
    return member + " is an instructor";
});

console.log(instructors);
console.log(members);

// map() returns a new array that contains value/data returned by the function that was for each item in the original array

let numArr2 = [1, 2, 3, 4, 5];

let squareMap = numArr2.map(function(number) {
    return number * number;
});

console.log(squareMap);

// includes() returns boolean which determines is the item is in the arrat or not

 //map() vs forEach()
 //map() is able to return a new array
 //forEach() simply iterates and does not return anything

let squareForEach = numArr2.forEach(function(number){

    return number * number;

});

console.log(squareForEach);


// includes() - returns bool value if item is in array
// include can be used in conditional statement

let isAMember = members.includes("Tine");
console.log(isAMember);

let isAMember2 = members.includes("Tee Jae");
console.log(isAMember2);

// arrayName.method()

/*

push() - add at the end
pop() - remove at the emd
shift() - remove at beginning
unshift() - add  beginning

*/

